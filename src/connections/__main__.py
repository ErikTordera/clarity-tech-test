import argparse

from .services.get_connections_between import get_connections_between
from .services.get_connections_live import get_connections_live


parser = argparse.ArgumentParser(
    description='Process connection logs and get simplified connections'
)

parser.add_argument(
    '-i',
    '--input-file',
    type=argparse.FileType('r'),
    help='File to parse from',
    required=True
)

parser.add_argument(
    '-init',
    '--init-date',
    type=int,
    default=0,
    help='Init date to show connection logs, only usable in the synchronous version',
    required=False
)

parser.add_argument(
    '-end',
    '--end-date',
    type=int,
    default=0,
    help='End date to show connection logs, only usable in the synchronous version',
    required=False
)

parser.add_argument(
    '-host',
    '--hostname',
    type=str,
    default='',
    help='hostname to filter the log file',
    required=False
)

parser.add_argument(
    '-a',
    '--async',
    type=bool,
    default=False,
    help='Opens the file and asynchronously reads it and ' +
         'outputs every hour the connections received',
    required=False
)

if __name__ == '__main__':
    args = vars(parser.parse_args())

    if args['async']:
        get_connections_live(
          hostname=args['hostname'],
          file=args['input_file']
        )
    else:
        get_connections_between(
          init_date=int(args['init_date']),
          end_date=int(args['end_date']),
          hostname=args['hostname'],
          file=args['input_file']
        )
