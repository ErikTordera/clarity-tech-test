def get_connections_between(init_date: int, end_date: int, hostname: str, file):
    _margin = 300
    line = file.readline()
    while line != '':
        line = line.replace('\n', '').split(' ')
        timestamp = int(line[0])
        host_connected = line[1]
        host_received = line[2]

        if timestamp > init_date - _margin:
            if host_received == hostname:
                print(host_connected)
        line = file.readline()
        if timestamp > end_date + _margin:
            line = ''
