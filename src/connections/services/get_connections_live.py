import datetime
import time


def get_connections_live(hostname: str, file):
    last_comprobation = datetime.datetime.now()
    report_every_s = 3600
    sleep_time_s = 0.250

    connections_made = {}
    connections_received = {}

    while True:
        line = file.readline()
        if line != '':
            line = line.replace('\n', '').split(' ')
            host_connected = line[1]
            host_received = line[2]

            __add_to_key(connections_made, host_received, host_connected)
            __add_to_key(connections_received, host_connected, host_received)

        else:
            time.sleep(sleep_time_s)

        if (datetime.datetime.now() - last_comprobation).seconds >= report_every_s:
            __report(received=connections_received, made=connections_made, selected_host=hostname)
            last_comprobation = datetime.datetime.now()
            connections_made = {}
            connections_received = {}


def __report(received: dict, made: dict, selected_host: str):
    if selected_host in made:
        print(f'------Connections made by {selected_host}-------')
        for received_host in made[selected_host]:
            print(received_host)

    if selected_host in received:
        print(f'------Connections received by {selected_host}-------')
        for made_host in received[selected_host]:
            print(made_host)

    max_connections = 0
    max_host = ''
    for host_connecting, host_connections in made.items():
        if len(host_connecting) > max_connections:
            max_connections = len(host_connections)
            max_host = host_connecting
    if max_connections > 0:
        print(f'Host with the maximun conexions: {max_host}')


def __add_to_key(dict_to_add: dict, key: str, value: str):
    if key in dict_to_add:
        dict_to_add[key].append(value)
    else:
        dict_to_add[key] = [value]
