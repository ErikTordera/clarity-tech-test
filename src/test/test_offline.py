from ..connections.services.get_connections_between import get_connections_between

__FILE_PATH = 'input-file-10000.txt'
__EMPTY_PATH = 'empty-input-file.txt'
__CONTROL_PATH = 'test-input-file.txt'

__RESOURCES_FOLDER = 'src/test/resources/'

def test_empty_file(capsys):
    captured = capsys.readouterr()
    with open(f'{__RESOURCES_FOLDER}/{__FILE_PATH}', 'r') as file:
        get_connections_between(1565647246869, 1565647247170, 'Khiem', file)

    expected_out = ''
    assert captured.out == expected_out


def test_control_file(capsys):
    with open(f'{__RESOURCES_FOLDER}/{__CONTROL_PATH}', 'r') as file:
        get_connections_between(1500000000000, 1500000000002, 'ex3', file)

    captured = capsys.readouterr()
    expected_out = 'ex1\n'
    assert captured.out == expected_out


def test_real_file(capsys):
    with open(f'{__RESOURCES_FOLDER}/{__FILE_PATH}', 'r') as file:
        get_connections_between(1565647228800, 1565647264400, 'Jadon', file)

    captured = capsys.readouterr()
    expected_out = 'Remiel\n'
    assert captured.out == expected_out
