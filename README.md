# CLARITY - TECHNICAL TEST

## Erik Tordera Bermejo

This is a technical test in wich I have to develop a CLI unitily that is able to parse a given input in the form:

|timestamp|user1|user2|
|---|---|---|
|1565733535520|Keatin|Alika|

In the src directory you will find the connections utility ('main' logic) and the test directory using pytest.

## Dependencies
1. python
2. (Opt) virtualenv
3. ```pip install -r requirements.txt```

## Run

```python -m src.connections -h``` will print you the help for the utility.

Using ```-a / --async``` will use the continuos parse utility while not specifying this argument will use the normal parse with datetime and host filers.

```-i / --input-file``` argument ables to specify the input file to use in either of the endpoints.

```-h / --hostname``` specifies the hostname to filter with.

```-a / --async``` If specified, the asyncronous and never ending endpoint will be used:  
     - Asynchronous endpoint: The file will be read and parsed and will be maintained open to read from, every hour, a report will be prompted in the standard output with the last hour connections
     - Non-Asynchronous endpoint: The file will be read, processed and closed. In this endpoint, the arguments ```-init``` and ```-end``` are required to filter the file by date and ```-host``` to filter the connections between any user and the specified username.

## Linting

Run ``` pylint src.connections && pylint src.test``` in the terminal

## Testing

Run ``` pytest ``` in the terminal


---

An example input data file is provided in ```./input-file.10000.txt```

The problem is specified in the pdf in the roo directory ```./DE_code_challenge.pdf```
